## Introduction

ConnatixPlayerSDK provides a native way to deliver successful video experiences to publishers

## Installation

```ruby
pod 'ConnatixPlayerSDK'
```

## Documentation

Please consult the official Connatix support page for the iOS Native SDK documentation

## License

MIT