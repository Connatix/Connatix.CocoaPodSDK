Pod::Spec.new do |s|

# 1
s.platform = :ios
s.name = "ConnatixPlayerSDKObjc"
s.summary = "Connatix Player SDK for iOS with Objective-C support"
s.requires_arc = true
    
# 2
s.version = "4.4.10"
    
# 3
s.license = 'MIT'
    
# 4
s.author = { "Connatix" => "ios-support@connatix.com" }
    
s.homepage = "https://connatix.com"
    
# 5
s.ios.deployment_target = '12.0'
s.dependency 'ConnatixAppMeasurement', '1.4.13'
s.dependency 'GoogleAds-IMA-iOS-SDK', '~> 3.22'
s.ios.vendored_frameworks = 'ConnatixPlayerSDKObjc.xcframework'
    
# 6
s.source = { :git => "https://gitlab.com/Connatix/Connatix.CocoaPodSDK.git", :tag => "#{s.version}" }
    
s.swift_version = "5.0"
    
end