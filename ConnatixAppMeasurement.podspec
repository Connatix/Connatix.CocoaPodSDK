Pod::Spec.new do |s|

# 1
s.platform = :ios
s.name = "ConnatixAppMeasurement"
s.summary = "Connatix Framework for App Measurement"
s.requires_arc = true
        
# 2
s.version = "1.5.3"
        
# 3
s.license = 'Apache-2.0'
        
# 4
s.author = { "Connatix" => "ios-support@connatix.com" }
s.homepage = "https://connatix.com"
        
# 5
s.ios.deployment_target = '12.0'
s.ios.vendored_frameworks = 'OMSDK_Connatix.xcframework'
        
# 6
s.source = { :git => "https://gitlab.com/Connatix/Connatix.CocoaPodSDK.git", :tag => "measurement/#{s.version}" }
s.swift_version = "5.0"
        
end