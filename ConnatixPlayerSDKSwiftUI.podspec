Pod::Spec.new do |s|

# 1
s.platform = :ios
s.name = "ConnatixPlayerSDKSwiftUI"
s.summary = "iOS native SDK for the Connatix player. SwiftUI bridge for the UIKit based ConnatixPlayerSDK"
s.requires_arc = true

# 2
s.version = "5.1.7"

# 3
s.license = 'MIT'

# 4
s.author = { "Connatix" => "ios-support@connatix.com" }

s.homepage = "https://connatix.com"

# 5
s.ios.deployment_target = '13.0'
s.dependency 'ConnatixPlayerSDK', '5.1.7'
s.ios.vendored_frameworks = 'ConnatixPlayerSDKSwiftUI.xcframework'

# 6
s.source = { :git => "https://gitlab.com/Connatix/Connatix.CocoaPodSDK.git", :tag => "swiftui/#{s.version}" }

s.swift_version = "5.0"

end