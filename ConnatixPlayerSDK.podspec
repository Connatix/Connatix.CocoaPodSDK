Pod::Spec.new do |s|

# 1
s.platform = :ios
s.name = "ConnatixPlayerSDK"
s.summary = "Connatix Player SDK for iOS"
s.requires_arc = true

# 2
s.version = "5.1.7"

# 3
s.license = 'MIT'

# 4
s.author = { "Connatix" => "ios-support@connatix.com" }

s.homepage = "https://connatix.com"

# 5
s.ios.deployment_target = '12.0'
s.dependency 'ConnatixAppMeasurement', '1.5.3'
s.dependency 'GoogleAds-IMA-iOS-SDK', '~> 3.22'
s.ios.vendored_frameworks = 'ConnatixPlayerSDK.xcframework'

# 6
s.source = { :git => "https://gitlab.com/Connatix/Connatix.CocoaPodSDK.git", :tag => "#{s.version}" }

s.swift_version = "5.0"

end